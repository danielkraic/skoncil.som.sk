var Date2Str = (function () {
    function Date2Str() {
    }
    Date2Str.getStr = function (val, formatString) {
        var ret;
        if (val === 0) {
            ret = "";
        }
        else if (val === 1) {
            ret = val + " " + formatString[0];
        }
        else if (val < 5) {
            ret = val + " " + formatString[1];
        }
        else {
            ret = val + " " + formatString[2];
        }
        return ret;
    };
    Date2Str.getYy = function (value) {
        return this.getStr(value, this.formatYy);
    };
    Date2Str.getMm = function (value) {
        return this.getStr(value, this.formatMm);
    };
    Date2Str.getDd = function (value) {
        return this.getStr(value, this.formatDd);
    };
    Date2Str.getHh = function (value) {
        return this.getStr(value, this.formatHh);
    };
    Date2Str.getMi = function (value) {
        return this.getStr(value, this.formatMi);
    };
    Date2Str.getSs = function (value) {
        return this.getStr(value, this.formatSs);
    };
    Date2Str.formatYy = ["rok", "roky", "rokov"];
    Date2Str.formatMm = ["mesiac", "mesiace", "mesiacov"];
    Date2Str.formatDd = ["deň", "dni", "dní"];
    Date2Str.formatHh = ["hodinu", "hodiny", "hodín"];
    Date2Str.formatMi = ["minútu", "minúty", "minút"];
    Date2Str.formatSs = ["sekundu", "sekundy", "sekúnd"];
    return Date2Str;
}());
var DateDiff = (function () {
    function DateDiff(startDate, endDate) {
        this.yy = 0;
        this.mm = 0;
        this.dd = 0;
        this.hh = 0;
        this.mi = 0;
        this.ss = 0;
        this.monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        this.getMonthDays = function (year, month) {
            return (month == 2 && year % 4 == 0) ? this.monthDays[month - 1] + 1 : this.monthDays[month - 1];
        };
        this.yy = endDate.getFullYear() - startDate.getFullYear();
        this.mm = endDate.getMonth() - startDate.getMonth();
        this.dd = endDate.getDate() - startDate.getDate();
        this.hh = endDate.getHours() - startDate.getHours();
        this.mi = endDate.getMinutes() - startDate.getMinutes();
        this.ss = endDate.getSeconds() - startDate.getSeconds();
        if (this.ss < 0) {
            this.ss += 60;
            --this.mi;
        }
        if (this.mi < 0) {
            this.mi += 60;
            --this.hh;
        }
        if (this.hh < 0) {
            this.hh += 24;
            --this.dd;
        }
        if (this.dd < 0) {
            this.dd += this.getMonthDays(startDate.getFullYear(), startDate.getMonth() + 1);
            --this.mm;
        }
        if (this.mm < 0) {
            this.mm += 12;
            --this.yy;
        }
    }
    DateDiff.prototype.toString = function () {
        var strs = [
            Date2Str.getYy(this.yy),
            Date2Str.getMm(this.mm),
            Date2Str.getDd(this.dd),
            Date2Str.getHh(this.hh),
            Date2Str.getMi(this.mi),
            Date2Str.getSs(this.ss)
        ];
        var res;
        var len = strs.length;
        for (var i = 0; i < len; i++) {
            if (!strs[i]) {
                continue;
            }
            if (!res) {
                res = strs[i];
            }
            else if (res && (i + 1) == strs.length) {
                res += " a " + strs[i];
            }
            else {
                res += ", " + strs[i];
            }
        }
        return res;
    };
    return DateDiff;
}());
var Countdown = (function () {
    function Countdown(toDate) {
        this.toDate = toDate;
    }
    Countdown.prototype.isValid = function () {
        var currentDate = new Date();
        return currentDate < this.toDate;
    };
    Countdown.prototype.getCountdownString = function () {
        var currentDate = new Date();
        var diff = new DateDiff(currentDate, this.toDate);
        return diff.toString();
    };
    return Countdown;
}());
